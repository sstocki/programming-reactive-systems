package kvstore

import akka.actor.{Actor, ActorRef, Cancellable, PoisonPill, Props}
import akka.event.LoggingReceive
import kvstore.Arbiter._

import scala.concurrent.duration._
import scala.language.postfixOps

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long)                extends Operation
  case class Get(key: String, id: Long)                   extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long)                                        extends OperationReply
  case class OperationFailed(id: Long)                                     extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props =
    Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Persistence._
  import Replica._
  import Replicator._

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  var kv: Map[String, String] = Map.empty
  // a map from secondary replicas to replicators
  var secondaries: Map[ActorRef, ActorRef] = Map.empty
  // the current set of replicators
  var replicators: Set[ActorRef] = Set.empty

  var expectedSeq: Long = 0L

  var persistence: ActorRef = _

  // map from sequence number to triplet of sender, request and cancellable
  var persistenceOperationMap: Map[Long, (ActorRef, Operation, Cancellable)] = Map.empty
  // map from sequence number to triplet of sender, request and cancellable
  var persistenceSnapshotMap: Map[Long, (ActorRef, Snapshot, Cancellable)] = Map.empty
  // map from sequence number to pair of client and set of secondary replicas
  var replicationOperationMap: Map[Long, (ActorRef, Set[ActorRef])] = Map.empty

  private def createPersistence(): Unit = {
    persistence = context.actorOf(persistenceProps)
  }

  override def preStart(): Unit = {
    createPersistence()
    arbiter ! Join
  }

//  override def postStop(): Unit = {
//    persistence ! PoisonPill
//  }

//  override val supervisorStrategy = OneForOneStrategy() {
//    case _: PersistenceException =>
//      Restart
//    case _ =>
//      createPersistence()
//      Stop
//  }

  def receive: Receive = LoggingReceive {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = LoggingReceive {
    case insert @ Insert(key, value, id) =>
      ins(key, value)
      val replicate: Replicate = Replicate(key, Option(value), id)
      val persist: Persist     = Persist(key, Option(value), id)
      persistenceOperationMap = persistenceOperationMap + (id -> (sender, insert, schedulePersist(persist)))
      triggerReplication(id, sender, replicate)
      scheduleOnceDelayedFailure(OperationFailed(id))
    case remove @ Remove(key, id) =>
      rm(key)
      val replicate: Replicate = Replicate(key, None, id)
      val persist: Persist     = Persist(key, None, id)
      persistenceOperationMap = persistenceOperationMap + (id -> (sender, remove, schedulePersist(persist)))
      scheduleOnceDelayedFailure(OperationFailed(id))
      triggerReplication(id, sender, replicate)
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case Persisted(key, id) =>
      persistenceOperationMap.get(id) match {
        case Some((client, operation, cancellable)) if operation.key == key =>
          cancellable.cancel()
          persistenceOperationMap = persistenceOperationMap - id
          if (replicationOperationMap.contains(id)) {
            //do nothing
          } else {
            client ! OperationAck(id)
          }
      }
    case Replicated(_, id) =>
      replicationOperationMap.get(id) match {
        case Some((client, replicas)) =>
          val replicaFromSender: ActorRef   = secondaries.filter(_._2 == sender).keySet.head
          val newSecondaries: Set[ActorRef] = replicas - replicaFromSender
          if (newSecondaries.isEmpty) {
            replicationOperationMap = replicationOperationMap - id
            if (persistenceOperationMap.contains(id)) {
              //do nothing
            } else {
              client ! OperationAck(id)
            }
          } else {
            replicationOperationMap = replicationOperationMap + (id -> (client, newSecondaries))
          }
        case None => //do nothing
      }
    case operationFailed @ OperationFailed(id) =>
      val failedSent: Boolean = persistenceOperationMap.get(id) match {
        case Some((client, _, cancellable)) =>
          cancellable.cancel()
          persistenceOperationMap = persistenceOperationMap - id
          client ! operationFailed
          true
        case None =>
          false //do nothing
      }
      replicationOperationMap.get(id) match {
        case Some((client, _)) =>
          replicationOperationMap = replicationOperationMap - id
          if (!failedSent) client ! operationFailed
        case None => //do nothing
      }
    case Replicas(replicas) =>
      val newReplicas: Set[ActorRef] = replicas.filterNot(r => secondaries.contains(r) || r == self)
      val newReplicasMap: Map[ActorRef, ActorRef] =
        newReplicas.map(r => r -> context.actorOf(Replicator.props(r))).toMap
      val removedReplicasMap: Map[ActorRef, ActorRef] =
        secondaries.filterNot(r => replicas.contains(r._1) || r._1 == self)
      val removedReplicas    = removedReplicasMap.keySet
      val removerReplicators = removedReplicasMap.values.toSet

      val newReplicationMap = replicationOperationMap.flatMap {
        case (id, (client, secondaries)) =>
          val newSecondaries = secondaries -- removedReplicas
          if (newSecondaries.isEmpty) {
            client ! OperationAck(id)
            replicationOperationMap - id
          } else {
            replicationOperationMap + (id -> (client, newSecondaries))
          }
      }
      replicationOperationMap = newReplicationMap

      secondaries = (secondaries ++ newReplicasMap) -- removedReplicas
      replicators = (replicators ++ newReplicasMap.values.toSet) -- removerReplicators
      removedReplicas.foreach(_ ! PoisonPill)
      removerReplicators.foreach(_ ! PoisonPill)
      kv.foreach {
        case (k, v) =>
          val replicate: Replicate = Replicate(k, Option(v), expectedSeq)
          newReplicasMap.values.foreach(_ ! replicate)
          expectedSeq = expectedSeq + 1
      }
  }

  /* TODO Behavior for the replica role. */
  val replica: Receive = LoggingReceive {
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case snapshot @ Snapshot(key, valueOption, seq) =>
      if (seq > expectedSeq) {
        //ignore
      } else if (seq < expectedSeq) {
        updateExpectedSeq(seq)
        sender ! SnapshotAck(key, seq)
      } else {
        updateExpectedSeq(seq)
        valueOption match {
          case Some(value) => ins(key, value)
          case None        => rm(key)
        }
        val persist: Persist = Persist(key, valueOption, seq)
        persistenceSnapshotMap = persistenceSnapshotMap + (seq -> (sender, snapshot, schedulePersist(persist)))
      }
    case Persisted(key, id) =>
      persistenceSnapshotMap.get(id) match {
        case Some((replicator, snapshot, cancellable)) if snapshot.key == key =>
          cancellable.cancel()
          persistenceSnapshotMap = persistenceSnapshotMap - id
          replicator ! SnapshotAck(snapshot.key, snapshot.seq)
      }
  }

  private def updateExpectedSeq(seq: Long): Unit = {
    val seqInc = seq + 1
    if (expectedSeq < seqInc) expectedSeq = seqInc
  }

  private def ins(key: String, value: String): Unit = {
    kv = kv + (key -> value)
  }

  private def rm(key: String): Unit = {
    kv = kv - key
  }

  private def triggerReplication(id: Long, sender: ActorRef, replicate: Replicate): Unit = {
    if (secondaries.nonEmpty) {
      secondaries.values.foreach(_ ! replicate)
      replicationOperationMap = replicationOperationMap + (id -> (sender, secondaries.keySet))
    }
  }

  private def schedulePersist(persist: Persist): Cancellable =
    context.system.scheduler.scheduleWithFixedDelay(initialDelay = Duration.Zero,
                                                    delay = 100 milliseconds,
                                                    receiver = persistence,
                                                    message = persist)(context.system.dispatcher, self)

  private def scheduleOnceDelayedFailure(operationFailed: OperationFailed): Cancellable =
    context.system.scheduler
      .scheduleOnce(delay = 1 second, receiver = self, message = operationFailed)(context.system.dispatcher, self)
}
