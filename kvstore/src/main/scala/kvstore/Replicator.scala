package kvstore

import akka.actor.{Actor, ActorRef, Cancellable, Props}

import scala.concurrent.duration._
import scala.language.postfixOps

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to triplet of sender, request and cancellable
  var acks: Map[Long, (ActorRef, Replicate, Cancellable)] = Map.empty
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  // var pending = Vector.empty[Snapshot]

  var _seqCounter: Long = 0L
  def nextSeq(): Long = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  def receive: Receive = {
    case replicate @ Replicate(key, valueOption, _) =>
      val cancellable: Cancellable =
        context.system.scheduler.scheduleWithFixedDelay(
          initialDelay = Duration.Zero,
          delay = 200 milliseconds,
          receiver = replica,
          message = Snapshot(key, valueOption, _seqCounter))(context.system.dispatcher, self)
      acks = acks + (_seqCounter -> (sender, replicate, cancellable))
      nextSeq()
    case SnapshotAck(key, seq) =>
      acks.get(seq) match {
        case Some((senderActorRef, replicate, cancellable)) if replicate.key == key =>
          cancellable.cancel()
          acks = acks - seq
          senderActorRef ! Replicated(replicate.key, replicate.id)
        case None => //do nothing
      }
  }
}
