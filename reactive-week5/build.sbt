scalaVersion := "2.13.1"

val akkaVersion = "2.6.0"

name := "reactive-week5"
description := "Typed Actors: Introduction to Protocols," +
  "Protocols in Akka Typed, " +
  "Testing Akka Typed Behaviors, " +
  "Akka Typed Facilities, " +
  "Akka Typed Persistence, " +
  "Supervision revisited."

libraryDependencies ++= Seq(
  "com.typesafe.akka"        %% "akka-actor-typed"         % akkaVersion,
  "com.typesafe.akka"        %% "akka-persistence-typed"   % akkaVersion,
  "com.typesafe.akka"        %% "akka-actor-testkit-typed" % akkaVersion % Test,
  "org.scalatest"            %% "scalatest"                % "3.0.8" % Test
)
