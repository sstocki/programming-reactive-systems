/**
  * Copyright (C) 2009-2015 Typesafe Inc. <http://www.typesafe.com>
  */
package actorbintree

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.junit.Assert._
import org.junit.Test

import scala.concurrent.duration._
import scala.util.Random

class BinaryTreeSuite
    extends TestKit(ActorSystem("BinaryTreeSuite"))
    with ImplicitSender {

  import actorbintree.BinaryTreeSet._

  def receiveN(requester: TestProbe,
               ops: Seq[Operation],
               expectedReplies: Seq[OperationReply]): Unit =
    requester.within(5.seconds) {
      val repliesUnsorted = for (i <- 1 to ops.size)
        yield
          try {
            requester.expectMsgType[OperationReply]
          } catch {
            case ex: Throwable if ops.size > 10 =>
              sys.error(s"failure to receive confirmation $i/${ops.size}\n$ex")
            case ex: Throwable =>
              sys.error(
                s"failure to receive confirmation $i/${ops.size}\nRequests:" + ops
                  .mkString("\n    ", "\n     ", "") + s"\n$ex")
          }
      val replies = repliesUnsorted.sortBy(_.id)
      if (replies != expectedReplies) {
        val pairs = (replies zip expectedReplies).zipWithIndex filter (x =>
          x._1._1 != x._1._2)
        fail(
          "unexpected replies:" + pairs
            .map(x => s"at index ${x._2}: got ${x._1._1}, expected ${x._1._2}")
            .mkString("\n    ", "\n    ", ""))
      }
    }

  def verify(probe: TestProbe,
             ops: Seq[Operation],
             expected: Seq[OperationReply]): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])

    ops foreach { op =>
      topNode ! op
    }

    receiveN(probe, ops, expected)
    // the grader also verifies that enough actors are created
  }

  @Test def insert(): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])

    topNode ! Contains(testActor, id = 1, 1)
    expectMsg(ContainsResult(1, result = false))

    topNode ! Insert(testActor, id = 2, 1)
    topNode ! Insert(testActor, id = 3, 10)
    topNode ! Insert(testActor, id = 4, 5)
    topNode ! Insert(testActor, id = 5, 6)
    topNode ! Insert(testActor, id = 6, 7)
    topNode ! Insert(testActor, id = 7, 8)
    topNode ! Contains(testActor, id = 8, 8)

    expectMsg(OperationFinished(2))
    expectMsg(OperationFinished(3))
    expectMsg(OperationFinished(4))
    expectMsg(OperationFinished(5))
    expectMsg(OperationFinished(6))
    expectMsg(OperationFinished(7))
    expectMsg(ContainsResult(8, result = true))
    ()
  }

  @Test def insertMultipleTimes(): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])

    topNode ! Insert(testActor, id = 1, 5)
    topNode ! Insert(testActor, id = 2, 5)
    topNode ! Contains(testActor, id = 3, 5)

    expectMsg(OperationFinished(1))
    expectMsg(OperationFinished(2))
    expectMsg(ContainsResult(3, result = true))
    ()
  }

  @Test def remove(): Unit = {
    val requester = TestProbe()
    val requesterRef = requester.ref
    val ops = List(
      Insert(requesterRef, id = 100, 1),
      Insert(requesterRef, id = 101, 2),
      Insert(requesterRef, id = 102, 3),
      Insert(requesterRef, id = 103, 4),
      Contains(requesterRef, id = 50, 1),
      Remove(requesterRef, id = 10, 1),
      Remove(requesterRef, id = 11, 2),
      Remove(requesterRef, id = 12, 3),
      Contains(requesterRef, id = 80, 1),
      Contains(requesterRef, id = 81, 2),
      Contains(requesterRef, id = 82, 3)
    )

    val expectedReplies = List(
      OperationFinished(id = 10),
      OperationFinished(id = 11),
      OperationFinished(id = 12),
      ContainsResult(id = 50, result = true),
      ContainsResult(id = 80, result = false),
      ContainsResult(id = 81, result = false),
      ContainsResult(id = 82, result = false),
      OperationFinished(id = 100),
      OperationFinished(id = 101),
      OperationFinished(id = 102),
      OperationFinished(id = 103)
    )

    verify(requester, ops, expectedReplies)
  }

  @Test def insertAfterBeingRemoved(): Unit = {
    val requester = TestProbe()
    val requesterRef = requester.ref
    val ops = List(
      Insert(requesterRef, id = 100, 1),
      Contains(requesterRef, id = 101, 1),
      Remove(requesterRef, id = 102, 1),
      Contains(requesterRef, id = 103, 1),
      Insert(requesterRef, id = 104, 1),
      Contains(requesterRef, id = 105, 1)
    )

    val expectedReplies = List(
      OperationFinished(100),
      ContainsResult(101, result = true),
      OperationFinished(102),
      ContainsResult(103, result = false),
      OperationFinished(104),
      ContainsResult(105, result = true)
    )

    verify(requester, ops, expectedReplies)
  }

  @Test def gcWithoutRemove(): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])

    topNode ! Contains(testActor, id = 1, 1)
    expectMsg(ContainsResult(1, result = false))

    topNode ! Insert(testActor, id = 2, 1)
    topNode ! Insert(testActor, id = 3, 10)
    topNode ! Insert(testActor, id = 4, 5)
    topNode ! Insert(testActor, id = 5, 6)
    topNode ! Insert(testActor, id = 6, 7)
    topNode ! Insert(testActor, id = 7, 8)
    topNode ! Contains(testActor, id = 8, 8)
    topNode ! GC
    topNode ! Contains(testActor, id = 8, 8)

    expectMsg(OperationFinished(2))
    expectMsg(OperationFinished(3))
    expectMsg(OperationFinished(4))
    expectMsg(OperationFinished(5))
    expectMsg(OperationFinished(6))
    expectMsg(OperationFinished(7))
    expectMsg(ContainsResult(8, result = true))
    expectMsg(ContainsResult(8, result = true))
    ()
  }

  @Test def gcWithRemove(): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])
    topNode ! Insert(testActor, id = 100, 1)
    expectMsg(OperationFinished(100))

    topNode ! Contains(testActor, id = 101, 1)
    expectMsg(ContainsResult(101, result = true))

    topNode ! Remove(testActor, id = 102, 1)
    expectMsg(OperationFinished(102))

    topNode ! Contains(testActor, id = 103, 1)
    expectMsg(ContainsResult(103, result = false))

    topNode ! GC

    topNode ! Contains(testActor, id = 105, 1)
    expectMsg(ContainsResult(105, result = false))
    ()
  }

  @Test def gcWithPendingOperations(): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])

    topNode ! Contains(testActor, id = 1, 1)
    expectMsg(ContainsResult(1, result = false))

    topNode ! Insert(testActor, id = 2, 1)
    expectMsg(OperationFinished(2))

    topNode ! Insert(testActor, id = 3, 10)
    expectMsg(OperationFinished(3))

    topNode ! Insert(testActor, id = 4, 5)
    expectMsg(OperationFinished(4))

    topNode ! Insert(testActor, id = 5, 6)
    expectMsg(OperationFinished(5))

    topNode ! GC

    topNode ! Insert(testActor, id = 6, 7)
    expectMsg(OperationFinished(6))

    topNode ! Insert(testActor, id = 7, 8)
    expectMsg(OperationFinished(7))

    topNode ! Contains(testActor, id = 8, 8)
    expectMsg(ContainsResult(8, result = true))
    ()
  }

  @Test def `proper inserts and lookups (5pts)`(): Unit = {
    val topNode = system.actorOf(Props[BinaryTreeSet])

    topNode ! Contains(testActor, id = 1, 1)
    expectMsg(ContainsResult(1, result = false))

    topNode ! Insert(testActor, id = 2, 1)
    topNode ! Contains(testActor, id = 3, 1)

    expectMsg(OperationFinished(2))
    expectMsg(ContainsResult(3, result = true))
    ()
  }

  @Test def `instruction example (5pts)`(): Unit = {
    val requester = TestProbe()
    val requesterRef = requester.ref
    val ops = List(
      Insert(requesterRef, id = 100, 1),
      Contains(requesterRef, id = 50, 2),
      Remove(requesterRef, id = 10, 1),
      Insert(requesterRef, id = 20, 2),
      Contains(requesterRef, id = 80, 1),
      Contains(requesterRef, id = 70, 2)
    )

    val expectedReplies = List(
      OperationFinished(id = 10),
      OperationFinished(id = 20),
      ContainsResult(id = 50, result = false),
      ContainsResult(id = 70, result = true),
      ContainsResult(id = 80, result = false),
      OperationFinished(id = 100)
    )

    verify(requester, ops, expectedReplies)
  }

  @Test def `behave identically to built-in set (includes GC) (40pts)`()
    : Unit = {
    val rnd = new Random()
    def randomOperations(requester: ActorRef, count: Int): Seq[Operation] = {
      def randomElement: Int = rnd.nextInt(100)
      def randomOperation(requester: ActorRef, id: Int): Operation =
        rnd.nextInt(4) match {
          case 0 => Insert(requester, id, randomElement)
          case 1 => Insert(requester, id, randomElement)
          case 2 => Contains(requester, id, randomElement)
          case 3 => Remove(requester, id, randomElement)
        }

      for (seq <- 0 until count) yield randomOperation(requester, seq)
    }

    def referenceReplies(operations: Seq[Operation]): Seq[OperationReply] = {
      var referenceSet = Set.empty[Int]
      def replyFor(op: Operation): OperationReply = op match {
        case Insert(_, seq, elem) =>
          referenceSet = referenceSet + elem
          OperationFinished(seq)
        case Remove(_, seq, elem) =>
          referenceSet = referenceSet - elem
          OperationFinished(seq)
        case Contains(_, seq, elem) =>
          ContainsResult(seq, referenceSet(elem))
      }

      for (op <- operations) yield replyFor(op)
    }

    val requester = TestProbe()
    val topNode = system.actorOf(Props[BinaryTreeSet])
    val count = 1000

    val ops = randomOperations(requester.ref, count)
    val expectedReplies = referenceReplies(ops)

    ops foreach { op =>
      topNode ! op
      if (rnd.nextDouble() < 0.1) topNode ! GC
    }
    receiveN(requester, ops, expectedReplies)
  }
}
