/**
  * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
  */
package actorbintree

import akka.actor._
import akka.event.LoggingReceive

import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection */
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}

class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef =
    context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root: ActorRef = createRoot

  // optional (used to stash incoming operations during garbage collection)
  var pendingQueue: Queue[Operation] = Queue.empty[Operation]

  // optional
  def receive: Receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = LoggingReceive {
    case Contains(requester, id, elem) =>
      root ! Contains(requester, id, elem)
    case Insert(requester, id, elem) =>
      root ! Insert(requester, id, elem)
    case Remove(requester, id, elem) =>
      root ! Remove(requester, id, elem)
    case GC =>
      val newRoot: ActorRef = createRoot
      context.become(garbageCollecting(newRoot))
      root ! CopyTo(newRoot)
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = LoggingReceive {
    case CopyFinished =>
      root ! PoisonPill
      root = newRoot
      context.become(normal)
      pendingQueue.foreach(root ! _)
      pendingQueue = Queue.empty[Operation]
    case op: Operation =>
      pendingQueue = pendingQueue.appended(op)
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)

  /**
    * Acknowledges that a copy has been completed. This message should be sent
    * from a node to its parent, when this node and all its children nodes have
    * finished being copied.
    */
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) =
    Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees: Map[Position, ActorRef] = Map[Position, ActorRef]()
  var removed: Boolean = initiallyRemoved

  // optional
  def receive: Receive = normal

  private def removeElemOrSend(position: Position, rm: Remove): Unit = {
    if (subtrees.contains(position)) {
      subtrees(position) ! rm
    } else {
      rm.requester ! OperationFinished(rm.id)
    }
  }

  private def containsElemOrSend(position: Position, con: Contains): Unit = {
    if (subtrees.contains(position)) {
      subtrees(position) ! con
    } else {
      con.requester ! ContainsResult(con.id, result = false)
    }
  }

  private def insertElemOrSend(position: Position, ins: Insert): Unit = {
    if (subtrees.contains(position)) {
      subtrees(position) ! ins
    } else {
      subtrees += (position -> context.actorOf(
        BinaryTreeNode.props(ins.elem, initiallyRemoved = false)))
      ins.requester ! OperationFinished(ins.id)
    }
  }

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = LoggingReceive {
    case Contains(requester, id, elemToContain) =>
      if (elem == elemToContain) {
        requester ! ContainsResult(id, result = !removed)
      } else if (elemToContain < elem) {
        containsElemOrSend(Left, Contains(requester, id, elemToContain))
      } else {
        containsElemOrSend(Right, Contains(requester, id, elemToContain))
      }
    case Insert(requester, id, elemToInsert) =>
      if (elem == elemToInsert) {
        removed = false
        requester ! OperationFinished(id)
      } else if (elemToInsert < elem) {
        insertElemOrSend(Left, Insert(requester, id, elemToInsert))
      } else {
        insertElemOrSend(Right, Insert(requester, id, elemToInsert))
      }
    case Remove(requester, id, elemToRemove) =>
      if (elem == elemToRemove) {
        removed = true
        requester ! OperationFinished(id)
      } else if (elemToRemove < elem) {
        removeElemOrSend(Left, Remove(requester, id, elemToRemove))
      } else {
        removeElemOrSend(Right, Remove(requester, id, elemToRemove))
      }
    case CopyTo(treeNode) =>
      if (!removed) {
        treeNode ! Insert(self, 1000 + elem, elem)
      }
      val insertConfirmed = removed
      val expected = subtrees.values.toSet
      expected.foreach(_ ! CopyTo(treeNode))
      notifyCopyFinishOr(expected,
                         insertConfirmed,
                         copying(expected, insertConfirmed))
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive =
    LoggingReceive {
      case CopyFinished =>
        val newExpected = expected - sender()
        notifyCopyFinishOr(newExpected,
                           insertConfirmed,
                           copying(newExpected, insertConfirmed))
      case OperationFinished(_) =>
        val newInsertConfirmed = true
        notifyCopyFinishOr(expected,
                           newInsertConfirmed,
                           copying(expected, newInsertConfirmed))
    }

  def notifyCopyFinishOr(expected: Set[ActorRef],
                         insertConfirmed: Boolean,
                         receive: Receive): Unit = {
    if (insertConfirmed && expected.isEmpty) {
      context.parent ! CopyFinished
    } else {
      context.become(receive)
    }
  }
}
