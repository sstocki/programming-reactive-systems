package reactive

import akka.actor.{Actor, ActorRef, Props}

class CounterMain extends Actor {
  val counter: ActorRef = context.actorOf(Props[Counter], "counter")

  counter ! "incr"
  counter ! "incr"
  counter ! "get"

  def receive: Receive = {
    case count: Int ⇒
      println(s"count was $count")
      context.stop(self)
  }
}