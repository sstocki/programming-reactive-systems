scalaVersion := "2.13.1"

val akkaVersion = "2.6.0"

name := "reactive-week4"
description := "Distributed Computing: " +
  "Actors Are Distributed, " +
  "Eventual Consistency, " +
  "Actor Composition, " +
  "Scalability, " +
  "Responsiveness."

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "com.ning" % "async-http-client" % "1.9.40",
  "org.jsoup" % "jsoup" % "1.8.1",
  "ch.qos.logback" % "logback-classic" % "1.1.4")

Test / parallelExecution := false
